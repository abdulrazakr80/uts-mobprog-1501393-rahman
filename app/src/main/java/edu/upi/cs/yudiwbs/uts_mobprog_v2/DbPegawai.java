package edu.upi.cs.yudiwbs.uts_mobprog_v2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
/*
   perlu dilengkapi
 */
public class DbPegawai {

    //class untuk menyimpan record
    public static class Pegawai {
        public String nama;

    }



    private  SQLiteDatabase db;
    private final OpenHelper dbHelper;


    public DbPegawai(Context c) {
        dbHelper =  new OpenHelper(c);
    }

    public void open() {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    //HATI-HATI nama tabelnya PEGAWAI, bukan MAHASISWA

    public void updatePegawai(String namaAsal, String namaTujuan) {
        ContentValues cv = new ContentValues();
        cv.put("NAMA",namaTujuan);
        db.update("PEGAWAI",cv,"NAMA=?",new String[]{namaAsal} );
    }

    public void deleteAll() {
        //lengkapi  lihat modul android db  halaman terakhir
        db.delete("PEGAWAI",null,null);
    }


    public void deletePegawai(String nama) {
        //lengkapi lihat modul android db halaman terakhir
        db.delete("PEGAWAI","NAMA=?",new String[]{nama});
    }

    public long insertPegawai(String nama) {
        //lengkapi
        ContentValues newValues = new ContentValues();
        newValues.put("NAMA", nama);
        return db.insert("PEGAWAI", null, newValues);

    }

    //ambil data mahasiswa berdasarkan nama
    public Pegawai getMahasiswa(String nama) {
        Cursor cur = null;
        Pegawai P = new Pegawai();

        //kolom yang diambil
        String[] cols = new String [] {"NAMA"};
        //parameter, akan mengganti ? pada NAMA=?
        String[] param  = {nama};

        cur = db.query("PEGAWAI",cols,"NAMA=?",param,null,null,null);

        if (cur.getCount()>0) {  //ada data? ambil
            cur.moveToFirst();
            P.nama = cur.getString(1);

        }
        cur.close();
        return P;
    }




    public ArrayList<Pegawai> getAllPegawai() {
        ArrayList<Pegawai> out = new ArrayList<>();

        //lengkapi
        Cursor cur = null;
        cur = db.rawQuery("SELECT nama FROM Pegawai Limit 10", null);
        if (cur.moveToFirst()) {
            do {
                Pegawai peg = new Pegawai();
                peg.nama = cur.getString(0);
                out.add(peg);
            } while (cur.moveToNext());
        }
        cur.close();

        return out;
    }


}
